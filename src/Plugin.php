<?php

namespace OpenSourceDeveloper\Accounts;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Plugin as ReaktrPlugin;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;

/**
 * Class Plugin
 *
 * @package OpenSourceDeveloper\Accounts
 */
class Plugin extends ReaktrPlugin
{

    public ?string $pluginName = 'Accounts';
    public ?string $pluginKey = 'accounts';
    public ?string $pluginVendorName = 'OpenSourceDeveloper';
    public ?string $pluginVendorKey = 'open-source-developer';
    public ?string $pluginVersion = 'v0.0.1';
    public ?string $pluginDescription = 'accounts with addresses and contacts';
    public ?string $pluginWebsiteUrl = 'https://www.reaktr.io/plugins/open-source-developer/accounts';
    public ?string $pluginDocumentationUrl = 'https://docs.reaktr.io/plugins/open-source-developer/accounts';
    public ?string $pluginSupportUrl = 'https://www.reaktr.io/plugins/open-source-developer/accounts';
    public ?string $pluginSupportEmail = 'support@opensourcedeveloper.co.uk';
    public ?string $pluginSupportPhone = null;
    public ?array $pluginDependencies = [
        'open-source-developer' => [
            'addresses',
            'contacts'
        ]
    ];

    public function register(): void
    {
        parent::register();
    }

    public function boot()
    {
        parent::boot();
    }

    public function registerBindings(): void
    {
    }

    public function registerProviders(): void
    {
    }

    public function registerMiddleware(): void
    {
    }

    public function registerRoutes(): void
    {
        $routingService = app(RoutingService::class);
        $routingService->registerApiRoutes('App\Plugins\Community\OpenSourceDeveloper\Accounts\Request\Controllers\Api', __DIR__."/Request/Routes/Api/v1.php", 1);
    }

    public function registerCommands(): void
    {
    }

    public function registerViews(): void
    {
    }

    public function registerMigrations(): void
    {
    }

    public function registerPublishableAssets(): void
    {
    }

}
